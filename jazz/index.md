# Jazz

## History

- http://teacher.scholastic.com/activities/bhistory/history_of_jazz.htm
- https://www.britannica.com/art/jazz
- https://www.liveabout.com/an-introduction-to-jazz-music-2039582
- https://www.theguardian.com/music/series/a-history-of-jazz
- https://www.scaruffi.com/history/jazz.html
- http://www.umsl.edu/~owsleydc/outline-of-jazz-history.html

## Real Books

- https://www.swiss-jazz.ch/partitions-real-book.htm
- https://minedit.com/jazz-real-book-pdf/
