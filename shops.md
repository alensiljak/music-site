# Shops

## Drums stores in Vienna

- Klangfarbe ([link](http://klangfarbe.at/))
- MSV ([link](http://www.m-s-v.eu/))
- Drum City ([link](http://www.drumcity.at/))
- Music Vidic ([link](http://musicvidic.com/)), online shop Musik Welt ([link](http://www.musik-welt.at/)); Margaretenstrasse 23, 1040
- Musik Gatermann ([link](http://www.musik-gattermann.at/))
- Haus der Musik ([link](http://www.hdm-shop.com/))
- Muziker ([link](https://www.muziker.at/)), store in Bratislava 

## Online-only

- Thomann ([link](http://thomann.de/))
- Drum Tec ([link](http://drum-tec.de/))
- Musik Produktiv ([link](http://www.musik-produktiv.at/))
- Bax Shop ([link](http://www.bax-shop.de/))
- Kirstein ([link](https://www.kirstein.de/)), dealer for XM;
- Starwood Sticks ([link](https://www.starwoodsticks.de/))

## Classifieds

- Willhaben ([drums](https://www.willhaben.at/iad/kaufen-und-verkaufen/marktplatz/musikinstrumente-noten/schlaginstrumente-6557))
- Shpock ([link](http://www.shpock.com/l/e-drum/))
- E-bay ([drums](http://www.ebay.at/sch/Drums-Percussion/180012/i.html?_catref=1))
- E-bay Kleinanzeigen, Germany ([link](https://www.ebay-kleinanzeigen.de/), [drums](https://www.ebay-kleinanzeigen.de/s-musikinstrumente/schlagzeug/k0c74))
- Picclick, Germany ([drums](http://picclick.de/Musikinstrumente/Drums-Percussion/))
- Flohmarkt ([link](http://flohmarkt.at/), [drums](http://www.flohmarkt.at/schlagzeug/))
- Alle kleine Anzeige ([link](http://www.allekleinanzeigen.at/freizeit/e-drum-gebraucht.html))

## Guitars

- [Music Man](http://music-man.com/)
