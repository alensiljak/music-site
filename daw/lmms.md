# LMMS

LMMS, which stands for Linux MultiMedia Studio, is a fantastic Open Source DAW. Despite the name, a Windows port exists. It is fairly stable and does not crash when running multiple instances of VST plugins.

This is my recommended DAW for playing e-drums.

Pros

- Open Source
- supports multiple MIDI input devices
- runs VST plugins
- stable

Cons

- Can listen to only one MIDI input device. The use of multiple MIDI devices requires multiple instances of LMMS.

## ASIO

By default, it is using an audio library that does not support ASIO. Use this for ASIO4ALL support. Simply replace the portaudio-2.dll with the downloaded one.

## MIDI Mapping

LMMS VeSTige instrument settings will display a little green mark for the base note. When using with Roland modules, the octave is offset by one. Simply move the green square one octave lower and the drum mappings will match the Roland defaults. Move the marker from A4 to A3 note.

## Multiple Tracks

To record multiple tracks, multiple instances of LMMS need to be run. Each can record only one MIDI instrument at a time.
