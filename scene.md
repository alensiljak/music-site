# Scene

Music scene (in Vienna, Austria).

For clubs in Vienna, see [here](http://austria.alensiljak.tk/entertainment/music).

## Jam Sessions

Some places that host jam sessions. First a few additional sources:

- [List on WienXtra](https://www.wienxtra.at/soundbase/infos-von-a-z/offene-buehnen-jams-und-karaoke/)

### Jazz

- [1019](https://www.1019jazzclub.at/), [fb](https://www.facebook.com/1019jazzclub/), every 2nd Wed
- BBQ, Siebenbrunner platz
- [Concerto](http://www.cafeconcerto.at/), [fb](https://www.facebook.com/cafeconcerto), Tue
- [Drakon](https://www.facebook.com/drakonbar/), Wed, Sun
- [Harmer's Bar](https://www.facebook.com/groups/755242204543045/), ~ irregular
- [Jazz in Schwechat](http://www.jazzinschwechat.at/), Thu
- [Jazz in Schönbach](https://www.xing.com/communities/posts/schoenbach-jam-session-in-wien-1000959277), Tue
- [Loop](https://loop.co.at) or [fb](https://www.facebook.com/loop.vienna), [program](https://loop.co.at/events/), U-Bahn Bogen 26, 1080 Vienna, Tuesdays (jazz) and Thursdays (gypsy session, every 2nd).
- [Public](https://www.pub-lic.at), "the place to be", [fb](https://www.facebook.com/Public-the-place-to-be-677756649087360/), [jam sessions group](https://www.facebook.com/groups/522363708252764/), Hofmühlgasse 11, 1060
- [Reigen](http://www.reigen.at/), Hadikgasse 62
- [Tunnel Vienna Live](http://www.tunnel-vienna-live.at/), Jazz & Groove, once a month
- [Zwe](http://www.zwe.cc/), [fb](https://www.facebook.com/jazzcafezwe/), Tue, Wed

### Blues, Country

- [list](https://blues.at/termine/) at blues.at
- [17Ten](http://www.17ten.at/termine.html), twice a month, Thu;
- Billabong Open Mic, [fb group](https://www.facebook.com/openmicnightfeat.rolandstinauer), Billabong, Parkring 4, 1010
- Cafe Carina, [acoustic sessions](https://www.facebook.com/Acousticsessionfeat.rolandstinauer/), once per month, Tue; Josefstädter Straße 84, 1080 Wien
- [Louisiana blues pub](https://blues.at/termine/); Wed
- [Blue Tomato](http://www.bluetomato.cc/), monthly, Edi Fenzl.
- Quattro bar, [fb](https://www.facebook.com/pages/category/Live-Music-Venue/Quattro-Bar-1168512466516176/); Währingerstrasse 167, 1180
- Tunnel Vienna Live, Blues & Roots Jam Session, once a month
- [Cafe Stadler](https://www.cafestadler.at/), Wiener Neustadt

## Live Music

- [IG Jazz list](http://www.ig-jazz.at/)

Alphabetical list of venues

- 1019 Jazz Club
- Cafe [7Stern](https://www.7stern.net/), [fb](https://www.facebook.com/cafe7stern/)
- [Bebop](http://bebop-daslokal.at/)
- Restaurant Beograd, [fb](https://www.facebook.com/Restaurant-Beograd-Wien-114152408465/), jazz session every first & second Friday, 21:00, Schikanedergasse 7
- [Blue Tomato](http://www.bluetomato.cc/)
- Cafe Carina, [fb](https://www.facebook.com/CafeCarina/)
- Concerto
- Davis
- Jazzland
- [The Loft](https://www.theloft.at/), Phat Jam
- Martinschlössl
- [Mi Barrio](https://www.mibarrio.at/), latin
- Miles Smiles
- [Neruda](https://www.neruda.at/), latin
- Reigen
- Sargfabrik
- [Tunnel](http://www.tunnel-vienna-live.at/), [program](https://www.tunnel-vienna-live.at/veranstaltungen/)
- Zwe

## Workshops

- [Jazz and Wine](http://www.jazzandwine.at/), Poysdorf
- [Jazzseminar Schönbach](http://www.fredwork.at/jazzseminar/)

## Band Search

- Musiker in deiner Stadt ([link](https://www.musiker-in-deiner-stadt.at/))
- Band Suche ([link](http://www.bandsuche.at/))
