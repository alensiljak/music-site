# Advanced Topics

Here are some topics I'd consider advanced.

- Polyphonic playing
- [Polyrhythms](polyrhythms.md)
- Broken Time

## Polyphonic Playing

Polyphonic or harmonic playing is playing multiple tones at the same time. Can be applied to fills but also rhythms, like latin.
This is best illustrated in Bob Moses' Living Music, part 1, at 1:05.

- [Living Music, part 1](https://www.youtube.com/watch?v=T_wWL4FwoKc) 
- [Living Music, part 2](https://www.youtube.com/watch?v=E-vloLIvE00)

## Broken Time

- [How to play broken time, pt.1](https://www.youtube.com/watch?v=OHkNb-3fkY8) by Tony Arco. References to his teacher, Bob Moses, here as well.
- [How to play broken time, pt.2](https://www.youtube.com/watch?v=B2_HHyEiAiA)
