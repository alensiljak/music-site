# Orchestration

This is, literally, what you play - how much, how often, how loud.

Interesting exercise from Mark Guiliana for exploring orchestration (see the link below):
The right hand plays snare, ride, floor tom.
The left hand plays snare, high tom, left cymbal, hi-hat.
This creates interesting combinations of sounds. Legs can play alternating L-R with the bass and hi-hat.

## Resources

- [Mark Guiliana: Discovering Orchestration Paths On The Drums](https://www.youtube.com/watch?v=uT3qXqt-1P8)
