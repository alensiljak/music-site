# Drums

Various drums-related resources.

* [Advanced Topics](advanced-topics.md)
* [Bass](/drums/bass)
* [Brushes](/drums/brushes)
* [Drummers](/drums/drummers)
* [Dynamics](/drums/dynamics)
* [e-Drums](/drums/edrums)
* [Education](/drums/education)
* [Exercises](/drums/exercises)
* [Extreme Drumming](extreme-drumming)
* [Grip](/drums/grip)
* [Improvisation](/drums/improvisation)
* [Hardware](/drums/hardware)
* [Notation](/drums/notation)
* [Orchestration](/drums/orchestration)
* [Pocket](/drums/pocket)
* [Polyrhythms](/drums/polyrhythms)
* [Practice](/drums/practice)
* [Practice Elements](/drums/practice-elements)
* [Recording](/drums/recording)
* [Rudiments](/drums/rudiments)
* [Solo](/drums/solo)
* [Stroke](/drums/stroke)
* [Styles](/drums/styles)
* [Technique](/drums/technique)
* [Time](/drums/time)
* [Tuning](/drums/tuning)
* [Videos](/drums/videos)
* [Warm-up](/drums/warmup)

## References

- [History of the Drumset](https://www.youtube.com/watch?v=BH-jVncTJbg)
