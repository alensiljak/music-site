# Recording

Everything related to recording the drums.

- [A Quick & Easy 4-Mic Setup for Drum Kits](https://www.soundpure.com/a/expert-advice/recording/4-mic-recorderman-setup-for-drum-kits/)
- [8 Microphone Drums Setup](https://www.soundpure.com/a/expert-advice/recording/8-microphone-drums-setup/)
