# Improvisation

Improvisation is creating music on the spot. It is not a reproduction of a prepared piece.
It does, however, consists of reproducing practiced patterns but focuses on the act of creation, playing what is required in that moment.

Many aspects of musical playing are active in improvisation. The most relevant ones might be:

- [Dynamics](dynamics), the level at which you play
- Subdivision, rates; 8th notes, 16th notes, triplets, etc.
- Orchestration, where on the instrument you play an idea
- Phrasing, where in time you play idea

## Resources

- [Improvisation in Music, Wikipedia](https://en.wikipedia.org/wiki/Improvisation#Music)
- [Musical Improvisation, Wikipedia](https://en.wikipedia.org/wiki/Musical_improvisation)
- [Mark Guiliana: Exploring Your Creativity On The Drums (FULL DRUM LESSON) - Drumeo](https://www.youtube.com/watch?v=5A0fw7-HKYE)
