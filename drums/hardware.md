# Hardware

## Bass Drum

- [Bass Drum Holes](http://drumheadauthority.com/articles/bass-drum-hole/)
- [Kick drum patches](http://drumheadauthority.com/product/kick-drum-patches/)
- [Bass Drum Pedal Cam Types](https://www.soundpure.com/a/expert-advice/drums/bass-drum-pedal-cam-types/)

## Cymbals

Cymbals are the most complex part of the drumset, the part that most uniquely identifies it's sound.

Some characteristics:
- weight
- size
- bell size
- curvature / profile
- material, alloy; B8, B20
- hammering & lathing patterns

Resources:

- Educational series from Sound Pure:
  - [Cast vs Sheet Cymbals](https://www.soundpure.com/a/expert-advice/drums/cast-vs-sheet-cymbals-what-you-need-to-know/)
  - [Cymbal Alloys 101](https://www.soundpure.com/a/expert-advice/drums/cymbal-alloys-101/)
  - [Effects of Hammering and Lathing Cymbals](https://www.soundpure.com/a/expert-advice/drums/hammering-lathing-cymbals/)
  - [Cymbal Anatomy 101](https://www.soundpure.com/a/expert-advice/drums/cymbal-anatomy-101/)

## Drumsticks

Drumsticks affect the way you play significantly, and they affect the sound that comes out.

The actual size of the stick grows as the number drops. B is thicker than A.

Sizes:
- 7A
- 5A
- 5B
- 2B

Shapes:
- Round; clean, bright and articulate
- Barrel; punchy and loud
- Acorn; full, rich and fat sound
- Oval; well-rounded frequency response
- Teardrop; focused low-end, warm and dark

Tip Material:
- wood; warmer and darker
- nylon; brighter and more articulate

Wood type:
- hickory
- maple; light
- acorn

- [How Drumstick Tips Can Affect Your Sound](https://www.soundpure.com/a/expert-advice/drums/how-drumstick-tips-affect-sound/)

## Snare Drum

Snare drums are created from wood and metal. Well, plexiglass, too.

- [Common Metal Snare Drum Shells](https://www.soundpure.com/a/expert-advice/drums/common-metal-snare-drum-shells/)