# Warm-up Exercises

Before any practice or playing, do some warmup/stretching exercises.

In a warm-up, focus on the motion. Make it fluid and let it feel good. Do not hold position nor test your limits.

## Routine

1. Jojo Mayer hand-clapping + feet (3 min.)
2. "Wrist stretch 1" (1 min.)
3. Propeller (1 min.), Arm Stretch
4. Half-Pretzel, stretch only one arm (10 times)
5. Extended Pretzel, stretch both arms (10 times)

### 1. Jojo Mayer Hand Clap

Jojo Mayer hand clapping:

<img src="/img/jojo-mayer-hand-clapping.png" height="250px" />

- Reference [video](https://www.youtube.com/watch?v=ZnICKtoG2Wg)

It is useful to combine this with the feet warm-up, playing the feet on 8th notes with the heel down.

### 2. Wrist Stretch

Stretch your arms in front, holding two sticks. Bend your wrists down and up continuously.

<img src="/img/StrForDownSide.jpg" /> <img src="/img/StrForUpSide.jpg" />

### 3. Propeller

Stretch your arm to the side, holding two sticks, extended (not parallel). Rotate them like a propeller in one direction, than other.

<img class="pull-right" src="/img/propeller.png" />

### 4. Half-Pretzel

Half-Pretzel is a variation of the Pretzel exercise where only one arm is stretched. This is done by having one hand pointing up and the other down in the initial position. 

The arm with the hand pointing up is the one being stretched. This one rotates.

The arm with the hand pointing down stays in the same position.

<img src="/img/pretzel-1.png" width="350px" /> <img src="/img/pretzel-2.png" width="350px" /> <img src="/img/pretzel-3.png" width="350px" />

### 5. Extended Pretzel

This is an extension of “the Pretzel”. After Pretzel stretch, lift your arms up vertically, then bow the elbows and put the sticks behind the head. Return to the initial position by doing the same motion in reverse. Repeat for 9 times. The last time keep the hands in position for ~10 seconds.

<img src="/img/extended-pretzel.png" />

- [Pretzel](https://flypaper.soundfly.com/write/wrist-and-hand-exercises-for-drummers/)

### Other

Another good stretch/warm-up tool is to just shake fingers, wrists, arms and shoulders.

For more exercises, see [Exercises](exercises). Exercises without sticks are also suitable as warmups.

#### Lower-arm Rotation

<img src="/img/rotate-lower-arm-bg.png" />

It is useful to warm-up, or generally exercise, the lower arm rotation. These muscles are used for better stick control and precise strikes. Try to rotate slowly and as much as you can. 

This is similar to the Propeller warm-up exercise above. In the Propeller exercise, the whole arm is being rotated. Here we rotate only the lower-arm. This can be achieved by holding arms next to your body, in the playing position, bent in the elbow so that only the lower arm is in horizontal position.


<img src="/img/person-arm-bent.png" />

## Stretching

Stretch the muscles after playing if they are stiff. See "Stretches for Drummers" link in the References.

- "Wrist Stretch 3" (aikido stretch), <br/><img src="/img/StrAkiR4Side.jpg" width="250px" />
- "Hand Stretch"

## Legs

Shin Warm-Up

Put your legs in a natural position. Lift the toes as high as you can, keeping the heel on the flor. Do quick taps, keeping the feet up. Play 8th notes (while hands do 16th notes). A good starting tempo would be 80bpm.

<img src="/img/shin-warmup.png" />

## References

- Warming up ([link](http://www.drummercafe.com/education/lessons/stretching-and-warming-up-the-body.html))
- Stretches for Drummers ([link](http://puppetista.org/drums/stretch.html))
- [Learn To Warm-Up And Stretch Before Drumming](http://www.rockdrummingsystem.com/underground/drum-articles/stretch-and-warm-up.php)
- [Drumming Warm-Up (Advanced)](https://www.youtube.com/watch?v=Orb8jofNRZ4)
- [Warming up with Steve Smith](https://www.moderndrummer.com/2017/08/warming-steve-smith/)
- [Stretches for Drummers](http://podestasportsmedicine.com/articles/stretches-for-drummers/), Podesta
