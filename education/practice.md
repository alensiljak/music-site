# Practice

In general, one could divide practice into

- Technical practice
- Musical practice

Moreover, there are many aspects one can work on:

- Technique
- Time
- Reading, sight reading
- Writing, transcribing
- Sound, tone
- Listening, hearing, ear training
- Orchestration
- Expression
- Dynamics
- Band playing = playing with others
- Improvisation = coming up and developing musical ideas
- Repertoire, memory = learning songs
- Music theory
- Music history - instrument, genres, players

The key to practice is *deliberate practice*. Not just spending time but focusing on the issue that needs to be improved.
More on deliberate practice - [here](https://www.drummersresource.com/deliberate-practice-with-daniel-glass/) and [here](https://www.bangthedrumschool.com/drum-practice-secrets-the-essential-elements-based-on-science/).
Based on the recent studies, the gist of the practice that produces the best results is:

- Isolate problem areas
- work on one specific issue slowly
- practice until you master the issue

## Links

- [How and What To Practice On Your Instrument](https://www.youtube.com/watch?v=33rNgjJ7c9Q), Rick Beato
  - Technique
  - Repertoire
  - Ear Training
  - Music Theory
  - Sight Reading
