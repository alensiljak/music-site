# Institutions

Berklee is one of the best known institutions to offer music education. They offer some courses online.

- Online courses ([link](https://online.berklee.edu/))
- On edX ([link](https://www.edx.org/school/berkleex))
- On Coursera ([link](https://www.coursera.org/berklee))

### Vienna

- Vienna Conservatory ([link](http://www.viennaconservatory.at/home)), jazz ([link](http://jazz.viennaconservatory.at/))
- Jazz Music Lab ([link](http://jammusiclab.at/))
- Music and Arts University ([link](http://www.muk.ac.at/en/studies/faculty-of-music/jazz/))
- Vienna Music Institute ([link](http://www.vmi.at/index_en.html))
- Prayner Conservatory ([link](http://www.konservatorium-prayner.at/))
- Campanella ([link](http://www.campanella-academy.com/?lang=en))

### Graz

- Kunst Uni, Jazz ([link](https://www.kug.ac.at/news-veranstaltungen/veranstaltungen/jazz.html))

## Online

- List of available courses at Class Central ([link](https://www.class-central.com/subject/music)). Includes Coursera, Kadenze, FutureLearn, etc.
- The 40 Best Sources of Free Music Education Online ([link](http://www.onlineuniversities.com/blog/2012/07/the-40-best-sources-free-music-education-online/))

### Providers

- BBC ([link](http://www.bbc.co.uk/learning/subjects/music.shtml))
- Coursera
- Dave Conservatoire ([link](http://www.daveconservatoire.org/))
- edX
- MIT Open Courseware ([link](https://ocw.mit.edu/courses/music-and-theater-arts/))
- Udemy
- Open University courses ([link](http://www.open.edu/openlearn/history-the-arts/culture/music))
- Kadenze ([link](https://www.kadenze.com/))
- Stanford Open edX
- Open Yale Courses ([link](http://oyc.yale.edu/music))
