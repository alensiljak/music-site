# Education

TOC:

- [Institutions](institutions)
- [Practice](practice)

## Program

A typical Jazz program at a conservatory would be:

- Musiklehre Jazz
- Solfeggio,
- Gehörbildung Jazz 1, 2
- Harmonielehre u. Stilkunde
- Jazztheorie 1
- Jazztheorie 2
- Stageband
- Jazzorchester
- Arrangement
- Solistenensemble 1
- Solistenensemble 2 (Impr. 2)
- Rhythmik (Freifach)
- Notationskunde (Freifach)
- Homerecording (Freifach)
- Stageband C - Impr. 1 (Freifach)

+ Freie Wahlfächer
+ Schwerpunkt nach Wahl:

- Theorie der Musik
- Ensembleleitung/Arrangement

## Drums

## References

## Seminars / Workshops

- Poysdorf ([link](https://www.jazzandwine.at/))
- Jazzseminar Schönbach ([link](http://www.fredwork.at/jazzseminar/))
