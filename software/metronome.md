# Metronome

The previous [list of metronomes](https://sites.google.com/site/alensmusicsite/home/software/metronome) is being slowly migrated here.

Source: Big list of metronomes ([link](http://robertinventor.com/bmwiki/Big_list_of_Windows_and_Online_Metronomes)).

## Online 

After being disappointed by a whole host of metronomes, I decided to write my own. It is the first one in the list below.

- [Alen's Metronome](http://metronome.alensiljak.ml/)
- [Flute Tunes](https://www.flutetunes.com/metronome/)
- [Online Stopwatch](https://www.online-stopwatch.com/metronome/)
- Best Metronomes ([link](http://www.bestmetronome.com/))
    - Accessible, has tap-to-bpm ([link](http://a.bestmetronome.com/))
    - Best Drum Trainer ([link](http://bestdrumtrainer.com/tt/))
    - Speed Trainer ([link](http://bestdrumtrainer.com/st/))
- [Session Town](https://www.sessiontown.com/en/music-games-apps/free-online-metronome-app)
- [Metronome Online](http://metronomeonline.com/)
- [Nedtronome](http://www.nedtronome.com/)
- [Gitori](http://gitori.com/metronome), can't get the sound

Open Source projects:

- metronome, mechanical [link](https://github.com/MisterY/metronome-mechanical)
- metronome, digital [link](https://github.com/MisterY/metronome-digital)

### BPM Tools

- Best Metronomes
- All 8, [link](http://www.all8.com/tools/bpm.htm)

## Linux

- GTick
- [KMetronome](http://kmetronome.sourceforge.net/)
- [Monoplugs](http://monoplugs.com/forum/viewtopic.php?t=958)

## Windows

- [PC 9 Virtual Metronome](http://www.chordpulse.com/pc-virtual-metronome.html)
- MuseScore (simply creating a score and turning on a metronome with intro)
- [TempoPerfect](https://www.nch.com.au/metronome/index.html)
- [Give me a Tac](http://givemetac.free.fr/index.php)
- [Bounce Metronome]

## Android

- Pro Metronome
- Drummer's Metronome ([link](https://play.google.com/store/apps/details?id=de.stefanpledl.drummersmetronome))
- Metronomerus ([link](https://play.google.com/store/apps/details?id=mh.knoedelbart.metronomerous))
- Metronomics ([link](http://metronomicsapp.com/))

## Resources

- [How to Practise With a Metronome in a Relaxed, Enjoyable & Precise Way](https://www.youtube.com/watch?v=B2ld_M2ascU&list=PLjdDsipyFoLXaGNoIA228KCVByWUd1bod)
